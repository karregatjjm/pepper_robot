<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Hello I am Pepper, today i will be posing with you to create photographic memories</source>
            <comment>Text</comment>
            <translation type="unfinished">Hello I am Pepper, today i will be posing with you to create photographic memories</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Hello I am Pepper, today i will be posing with you to create photographic memories</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello I am Pepper, today i will be posing with you to create photographic memories</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>I am ready for the next picture, please join me!</source>
            <comment>Text</comment>
            <translation type="unfinished">I am ready for the next picture, please join me!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (2)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Touch my tablet when everybody is ready!</source>
            <comment>Text</comment>
            <translation type="unfinished">Touch my tablet when everybody is ready!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (3)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Thank You! please enjoy your stay at TMC!</source>
            <comment>Text</comment>
            <translation type="unfinished">Thank You! please enjoy your stay at TMC!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (4)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Thank you! Don't forget to post the picture!</source>
            <comment>Text</comment>
            <translation type="unfinished">Thank you! Don't forget to post the picture!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (5)</name>
        <message>
            <source>Have you already cuddled with other robots?</source>
            <comment>Text</comment>
            <translation type="obsolete">Have you already cuddled with other robots?</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Have you already seen the play oh robots?</source>
            <comment>Text</comment>
            <translation type="unfinished">Have you already seen the play oh robots?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (6)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>have fun! upstairs there is also more fun!</source>
            <comment>Text</comment>
            <translation type="unfinished">have fun! upstairs there is also more fun!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (7)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>this was fun! you can also get some cotton candy!</source>
            <comment>Text</comment>
            <translation type="unfinished">this was fun! you can also get some cotton candy!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (8)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>I wish I could get a balloon animal upstairs!</source>
            <comment>Text</comment>
            <translation type="unfinished">I wish I could get a balloon animal upstairs!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Boxing/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Touch my head when the picture has been taken!</source>
            <comment>Text</comment>
            <translation type="unfinished">Touch my head when the picture has been taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Drinking Beer/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Touch my head when the picture has been taken!</source>
            <comment>Text</comment>
            <translation type="unfinished">Touch my head when the picture has been taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/HUG/Say</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Touch my head when the picture has been taken!</source>
            <comment>Text</comment>
            <translation type="unfinished">Touch my head when the picture has been taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Monkey/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Touch my head when the picture has been taken!</source>
            <comment>Text</comment>
            <translation type="unfinished">Touch my head when the picture has been taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Pepper_entrance/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Hi, I am Pepper</source>
            <comment>Text</comment>
            <translation type="obsolete">Hi, I am Pepper</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Pepper_entrance/Animated Say (1)</name>
        <message>
            <source>I was passing by an saw that a lot op people were gathered here</source>
            <comment>Text</comment>
            <translation type="obsolete">I was passing by an saw that a lot op people were gathered here</translation>
        </message>
        <message>
            <source>I was passing by an saw that a lot of people were gathered here</source>
            <comment>Text</comment>
            <translation type="obsolete">I was passing by an saw that a lot of people were gathered here</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Pepper_entrance/Animated Say (2)</name>
        <message>
            <source>Can someone tell  me what's going on here?</source>
            <comment>Text</comment>
            <translation type="obsolete">Can someone tell  me what's going on here?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Pretty_pose/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Touch my head when the picture has been taken!</source>
            <comment>Text</comment>
            <translation type="unfinished">Touch my head when the picture has been taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction (1)/Animated Say (1)</name>
        <message>
            <source>However, the stairs do not look very robot-friendly!</source>
            <comment>Text</comment>
            <translation type="obsolete">However, the stairs do not look very robot-friendly!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction (1)/Animated Say (2)</name>
        <message>
            <source>Can someone tell  me what's going on here?</source>
            <comment>Text</comment>
            <translation type="obsolete">Can someone tell  me what's going on here?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction (1)/Animated Say (3)</name>
        <message>
            <source>I will just move over here and start with the bitter bal len and beer</source>
            <comment>Text</comment>
            <translation type="obsolete">I will just move over here and start with the bitter bal len and beer</translation>
        </message>
        <message>
            <source>I will just move over here and start with the bitter bals and beer</source>
            <comment>Text</comment>
            <translation type="obsolete">I will just move over here and start with the bitter bals and beer</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction (1)/Animated Say (6)</name>
        <message>
            <source>Hi Ronald, nice to meet you!     I am glad to stay here and hear the rest of your speech!</source>
            <comment>Text</comment>
            <translation type="obsolete">Hi Ronald, nice to meet you!     I am glad to stay here and hear the rest of your speech!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction/Animated Say</name>
        <message>
            <source>What a nice place!</source>
            <comment>Text</comment>
            <translation type="obsolete">What a nice place!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction/Animated Say (1)</name>
        <message>
            <source>However, the stairs do not look very robot-friendly!</source>
            <comment>Text</comment>
            <translation type="obsolete">However, the stairs do not look very robot-friendly!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction/Animated Say (2)</name>
        <message>
            <source>Can someone tell  me what's going on here?</source>
            <comment>Text</comment>
            <translation type="obsolete">Can someone tell  me what's going on here?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction/Animated Say (3)</name>
        <message>
            <source>Let me properly introduce myself</source>
            <comment>Text</comment>
            <translation type="obsolete">Let me properly introduce myself</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction/Animated Say (4)</name>
        <message>
            <source>I come from Paris, but live in the Netherlands for a year now!</source>
            <comment>Text</comment>
            <translation type="obsolete">I come from Paris, but live in the Netherlands for a year now!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction/Animated Say (5)</name>
        <message>
            <source>I usually live at the T U E, where a also met a few TMC people!</source>
            <comment>Text</comment>
            <translation type="obsolete">I usually live at the T U E, where a also met a few TMC people!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction/Animated Say (6)</name>
        <message>
            <source>Hi Ronald, nice to meet you!     I am glad to stay here and hear the rest of your speech!</source>
            <comment>Text</comment>
            <translation type="obsolete">Hi Ronald, nice to meet you!     I am glad to stay here and hear the rest of your speech!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Proper Introduction/Animated Say (7)</name>
        <message>
            <source>But now thay you know me, i am interested in who you are.</source>
            <comment>Text</comment>
            <translation type="obsolete">But now thay you know me, i am interested in who you are.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Salute/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Touch my head when the picture has been taken!</source>
            <comment>Text</comment>
            <translation type="unfinished">Touch my head when the picture has been taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Hello I am Pepper, today i will be posing with you to create photographic memories</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello I am Pepper, today i will be posing with you to create photographic memories</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>I will salute!</source>
            <comment>Text</comment>
            <translation type="unfinished">I will salute!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (1)</name>
        <message>
            <source>I am ready for the next picture, please join me</source>
            <comment>Text</comment>
            <translation type="obsolete">I am ready for the next picture, please join me</translation>
        </message>
        <message>
            <source>Thank you! you picture will be ready shortly. please enjoy your stay at TMC!</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank you! you picture will be ready shortly. please enjoy your stay at TMC!</translation>
        </message>
        <message>
            <source>Thank You! please enjoy your stay at TMC!</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank You! please enjoy your stay at TMC!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (10)</name>
        <message>
            <source>Thank you! Don't forget to post the picture!</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank you! Don't forget to post the picture!</translation>
        </message>
        <message>
            <source>this was fun! you can also get some cotton candy!</source>
            <comment>Text</comment>
            <translation type="obsolete">this was fun! you can also get some cotton candy!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (11)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>I challenge you to a boxing match!</source>
            <comment>Text</comment>
            <translation type="unfinished">I challenge you to a boxing match!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (12)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>I feel pretty, oh so pretty!</source>
            <comment>Text</comment>
            <translation type="unfinished">I feel pretty, oh so pretty!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (13)</name>
        <message>
            <source>I wish I could get a balloon animal upstairs!</source>
            <comment>Text</comment>
            <translation type="obsolete">I wish I could get a balloon animal upstairs!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (14)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>come stand next to me!</source>
            <comment>Text</comment>
            <translation type="unfinished">come stand next to me!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (2)</name>
        <message>
            <source>I am ready for the next picture, please join me</source>
            <comment>Text</comment>
            <translation type="obsolete">I am ready for the next picture, please join me</translation>
        </message>
        <message>
            <source>pose 2</source>
            <comment>Text</comment>
            <translation type="obsolete">pose 2</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Act like a Monkey</source>
            <comment>Text</comment>
            <translation type="unfinished">Act like a Monkey</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (3)</name>
        <message>
            <source>please touch my head when the photograph is ready!</source>
            <comment>Text</comment>
            <translation type="obsolete">please touch my head when the photograph is ready!</translation>
        </message>
        <message>
            <source>please touch my head when the photograph has been taken</source>
            <comment>Text</comment>
            <translation type="obsolete">please touch my head when the photograph has been taken</translation>
        </message>
        <message>
            <source>please touch my tablet when the photograph has been taken</source>
            <comment>Text</comment>
            <translation type="obsolete">please touch my tablet when the photograph has been taken</translation>
        </message>
        <message>
            <source>act like you're drinking beer</source>
            <comment>Text</comment>
            <translation type="obsolete">act like you're drinking beer</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>act like you're having a drink!</source>
            <comment>Text</comment>
            <translation type="unfinished">act like you're having a drink!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (4)</name>
        <message>
            <source>I am ready for the next picture, please join me!</source>
            <comment>Text</comment>
            <translation type="obsolete">I am ready for the next picture, please join me!</translation>
        </message>
        <message>
            <source>Show me how strong you are!</source>
            <comment>Text</comment>
            <translation type="obsolete">Show me how strong you are!</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>fly like Super Man</source>
            <comment>Text</comment>
            <translation type="unfinished">fly like Super Man</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (5)</name>
        <message>
            <source>please touch my head when everybody is ready!</source>
            <comment>Text</comment>
            <translation type="obsolete">please touch my head when everybody is ready!</translation>
        </message>
        <message>
            <source>please touch my tablet when everybody is ready!</source>
            <comment>Text</comment>
            <translation type="obsolete">please touch my tablet when everybody is ready!</translation>
        </message>
        <message>
            <source>Touch my tablet when everybody is ready!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my tablet when everybody is ready!</translation>
        </message>
        <message>
            <source>Thank you! Don't forget to post the picture!</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank you! Don't forget to post the picture!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (6)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Do The Dab</source>
            <comment>Text</comment>
            <translation type="unfinished">Do The Dab</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (7)</name>
        <message>
            <source>Thank you! Don't forget to post the picture!</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank you! Don't forget to post the picture!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (8)</name>
        <message>
            <source>Thank you! Don't forget to post the picture!</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank you! Don't forget to post the picture!</translation>
        </message>
        <message>
            <source>Have you already cuddled with other robots?</source>
            <comment>Text</comment>
            <translation type="obsolete">Have you already cuddled with other robots?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (9)</name>
        <message>
            <source>Thank you! Don't forget to post the picture!</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank you! Don't forget to post the picture!</translation>
        </message>
        <message>
            <source>have fun! upstairs there is also more fun!</source>
            <comment>Text</comment>
            <translation type="obsolete">have fun! upstairs there is also more fun!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Strong/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Superman (1)/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Superman/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Touch my head when the picture has been taken!</source>
            <comment>Text</comment>
            <translation type="unfinished">Touch my head when the picture has been taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Working_version (1)/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Working_version (2)/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Touch my head when the picture has been taken!</source>
            <comment>Text</comment>
            <translation type="unfinished">Touch my head when the picture has been taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Working_version (4)/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Working_version/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/classic pose/Say</name>
        <message>
            <source>Touch my head when the picture is taken!</source>
            <comment>Text</comment>
            <translation type="obsolete">Touch my head when the picture is taken!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/react to building/Animated Say</name>
        <message>
            <source>What a nice place!</source>
            <comment>Text</comment>
            <translation type="obsolete">What a nice place!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/react to building/Animated Say (1)</name>
        <message>
            <source>I was passing by an saw that a lot op people were gathered here</source>
            <comment>Text</comment>
            <translation type="obsolete">I was passing by an saw that a lot op people were gathered here</translation>
        </message>
        <message>
            <source>However, the stairs do not look very robot-friendly!</source>
            <comment>Text</comment>
            <translation type="obsolete">However, the stairs do not look very robot-friendly!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/react to building/Animated Say (2)</name>
        <message>
            <source>Can someone tell  me what's going on here?</source>
            <comment>Text</comment>
            <translation type="obsolete">Can someone tell  me what's going on here?</translation>
        </message>
    </context>
</TS>
