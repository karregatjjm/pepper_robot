<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Photo_taking" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="Rondje_lab" src="Rondje_lab.pmt" />
        <File name="small_rotations" src="small_rotations.pmt" />
        <File name="MOVE_OPENING_1" src="MOVE_OPENING_1.pmt" />
        <File name="strafe_01" src="strafe_01.pmt" />
        <File name="TMC_logo" src="html/images/TMC_logo.jpg" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
