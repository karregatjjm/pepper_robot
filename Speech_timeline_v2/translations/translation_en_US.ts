<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Hi, I am Pepper</source>
            <comment>Text</comment>
            <translation type="obsolete">Hi, I am Pepper</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (1)</name>
        <message>
            <source> I started working as an employeneur at TMC about a year ago now and I am usually located at the T U E</source>
            <comment>Text</comment>
            <translation type="obsolete"> I started working as an employeneur at TMC about a year ago now and I am usually located at the T U E</translation>
        </message>
        <message>
            <source> I started working as an employeneur at TMC about a year ago.   I am usually located at the T U E</source>
            <comment>Text</comment>
            <translation type="obsolete"> I started working as an employeneur at TMC about a year ago.   I am usually located at the T U E</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue1/Animated Say</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Hi, I am Pepper</source>
            <comment>Text</comment>
            <translation type="unfinished">Hi, I am Pepper</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue1/Animated Say (1)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source> I started working as an employeneur at TMC about a year ago.   I am usually located at the T U E</source>
            <comment>Text</comment>
            <translation type="unfinished"> I started working as an employeneur at TMC about a year ago.   I am usually located at the T U E</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue1/Animated Say (2)</name>
        <message>
            <source>This is the first time I am at this location, is it always this busy here?</source>
            <comment>Text</comment>
            <translation type="obsolete">This is the first time I am at this location, is it always this busy here?</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>This is the first time I am at this location, Why is it so busy here?</source>
            <comment>Text</comment>
            <translation type="unfinished">This is the first time I am at this location, Why is it so busy here?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue2/Animated Say</name>
        <message>
            <source>oh Hello Ronald! </source>
            <comment>Text</comment>
            <translation type="obsolete">oh Hello Ronald! </translation>
        </message>
        <message>
            <source>ooow Hello Ronald! </source>
            <comment>Text</comment>
            <translation type="obsolete">ooow Hello Ronald! </translation>
        </message>
        <message>
            <source>Hello Ronald! what are you telling them about?</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello Ronald! what are you telling them about?</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Hello Ronald! what are you  talking about?</source>
            <comment>Text</comment>
            <translation type="unfinished">Hello Ronald! what are you  talking about?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue2/Animated Say (1)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>It looks really nice, all the windows give it such an open look!</source>
            <comment>Text</comment>
            <translation type="unfinished">It looks really nice, all the windows give it such an open look!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue2/Animated Say (2)</name>
        <message>
            <source>This is the first time I am at this location, is it always this busy here?</source>
            <comment>Text</comment>
            <translation type="obsolete">This is the first time I am at this location, is it always this busy here?</translation>
        </message>
        <message>
            <source>well.. not to be rude.. but the stairs do not look really robot friendly to me</source>
            <comment>Text</comment>
            <translation type="obsolete">well.. not to be rude.. but the stairs do not look really robot friendly to me</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>well.. not to be rude.. . . but the stairs do not look really robot friendly to me</source>
            <comment>Text</comment>
            <translation type="unfinished">well.. not to be rude.. . . but the stairs do not look really robot friendly to me</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue2/Animated Say (3)</name>
        <message>
            <source>I really like the new builidng!</source>
            <comment>Text</comment>
            <translation type="obsolete">I really like the new builidng!</translation>
        </message>
        <message>
            <source>Yes, because it is so nice i would love to!</source>
            <comment>Text</comment>
            <translation type="obsolete">Yes, because it is so nice i would love to!</translation>
        </message>
        <message>
            <source>Yes! it's so nice i would love to!</source>
            <comment>Text</comment>
            <translation type="obsolete">Yes! it's so nice i would love to!</translation>
        </message>
        <message>
            <source>Yes! it's so nice i would luuve to!</source>
            <comment>Text</comment>
            <translation type="obsolete">Yes! it's so nice i would luuve to!</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Yes! it's so nice i would lOve to!</source>
            <comment>Text</comment>
            <translation type="unfinished">Yes! it's so nice i would lOve to!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue2/Animated Say (4)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>can we arrange that I can get a desk with a view on the lake?</source>
            <comment>Text</comment>
            <translation type="unfinished">can we arrange that I can get a desk with a view on the lake?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue2/Animated Say (5)</name>
        <message>
            <source>...</source>
            <comment>Text</comment>
            <translation type="obsolete">...</translation>
        </message>
        <message>
            <location filename="../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../translations/behavior_1/behavior.xar" line="0"/>
            <source></source>
            <comment>Text</comment>
            <translation></translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue2/Animated Say (6)</name>
        <message>
            <location filename="../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../translations/behavior_1/behavior.xar" line="0"/>
            <source></source>
            <comment>Text</comment>
            <translation></translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3 (1)/Animated Say</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>I would really appriciate that!   </source>
            <comment>Text</comment>
            <translation type="unfinished">I would really appriciate that!   </translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3 (1)/Animated Say (1)</name>
        <message>
            <source>maybe I am the droiD he is looking for </source>
            <comment>Text</comment>
            <translation type="obsolete">maybe I am the droiD he is looking for </translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3 (1)/Animated Say (2)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>By the way Ronald</source>
            <comment>Text</comment>
            <translation type="unfinished">By the way Ronald</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3 (1)/Animated Say (3)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>even if I go there I would like to remain an employ eneur at T M C</source>
            <comment>Text</comment>
            <translation type="unfinished">even if I go there I would like to remain an employ eneur at T M C</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3 (1)/Animated Say (4)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Do you think there are possibilities to join them for a project?</source>
            <comment>Text</comment>
            <translation type="unfinished">Do you think there are possibilities to join them for a project?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3 (1)/Animated Say (5)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>that must be possible right?</source>
            <comment>Text</comment>
            <translation type="unfinished">that must be possible right?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3 (1)/Animated Say (6)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Do you think there are possibilities to join them for a project?</source>
            <comment>Text</comment>
            <translation type="unfinished">Do you think there are possibilities to join them for a project?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3 (1)/Animated Say (7)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>OK, then I will stay here and wait for the drinks and the bitter bal an</source>
            <comment>Text</comment>
            <translation type="unfinished">OK, then I will stay here and wait for the drinks and the bitter bal an</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3 (1)/Animated Say (8)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Do you think there are possibilities to join them for a project?</source>
            <comment>Text</comment>
            <translation type="unfinished">Do you think there are possibilities to join them for a project?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3/Animated Say</name>
        <message>
            <source>as you might expect, I really feel connected to the high-tech industry</source>
            <comment>Text</comment>
            <translation type="obsolete">as you might expect, I really feel connected to the high-tech industry</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>as you might expect, I am really connected to the high-tech industry</source>
            <comment>Text</comment>
            <translation type="unfinished">as you might expect, I am really connected to the high-tech industry</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3/Animated Say (1)</name>
        <message>
            <source>When I got here I saw another impressive building nearby, I think it was A S  L.</source>
            <comment>Text</comment>
            <translation type="obsolete">When I got here I saw another impressive building nearby, I think it was A S  L.</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>When I got here I saw another impressive building nearby, I think it was A S M  L.</source>
            <comment>Text</comment>
            <translation type="unfinished">When I got here I saw another impressive building nearby, I think it was A S M  L.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cue3/Animated Say (2)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Do you think there are possibilities to join them for a project?</source>
            <comment>Text</comment>
            <translation type="unfinished">Do you think there are possibilities to join them for a project?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
    </context>
</TS>
