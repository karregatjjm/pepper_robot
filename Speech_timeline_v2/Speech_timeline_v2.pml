<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Speech_timeline_v2" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="01_start" src="01_start.pmt" />
        <File name="02_rotation_on_spot" src="02_rotation_on_spot.pmt" />
        <File name="03_rotate_right_45" src="03_rotate_right_45.pmt" />
        <File name="04_rotate_left_45" src="04_rotate_left_45.pmt" />
        <File name="test" src="html/images/test.jpg" />
        <File name="TMC_logo" src="html/images/TMC_logo.jpg" />
        <File name="05_end_path" src="05_drive_off.pmt" />
        <File name="06_180_right_left" src="06_180_right_left.pmt" />
        <File name="07_final_move" src="07_final_move.pmt" />
        <File name="08_slow_right_25deg" src="08_slow_right_25deg.pmt" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
